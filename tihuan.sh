#!/bin/bash
sed -i "s/https:\/\/github.com/ssh:\/\/github.com/g" common-oscourse-qemuriscv64.yml 

cd  openembedded-core
sed -i "s/protocol=https/protocol=ssh/g"  `grep -rwl --include=*.bb --include=*.bbapend "github.com" `
cd  ../meta-riscv
sed -i "s/protocol=https/protocol=ssh/g" `grep -rwl --include=*.bb --include=*.bbapend "github.com" `

cd ../meta-openembedded
sed -i "s/protocol=https/protocol=ssh/g" `grep -rwl --include=*.bb --include=*.bbapend "github.com" `



